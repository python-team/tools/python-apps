===========================================
Python Applications Packaging Team - Policy
===========================================

:Author: Piotr Ożarowski <piotr@debian.org> (based on PMPT's policy)
:License: GNU GPL v2 or later

:Introduction:
  Python Applications Packaging Team is a sister of Python *Modules* Packaging
  Team. We are taking care of Python applications, i.e. programs written in
  Python that are not providing public modules.

  PAPT or just python-apps is hosted at salsa.debian.org, the Debian Gitlab
  installation. We currently have a GIT_repository_ and a mailing_list_ whose
  email address can be used in the Maintainer field on co-maintained packages.

  For more information send a message to: debian-python@lists.debian.org

.. contents::

----------------
Joining the team
----------------

The team is open to any python-related package maintainer. To be added to the
team, please request to join on debian-python_ mailing list.
Please indicate why you want to join the team: maintain your current packages
within the team, help maintain some specific packages, etc.

Any Python Modules Packaging Team member who wishes to integrate his packages
in the team can do so without requesting access (as the repository is writable
by all PMPT members and thus by all DD). If one wants to be more involved in
the team, we still recommend requesting access so that he appears in the public
member_list_ displayed on Salsa's project page. Debian developers: please note
if you're willing to act as a sponsor, you'll get a "sponsor" role on Alioth.

The team accepts all contributors and is not restricted to Debian developers.
Several Debian developers of the team will gladly sponsor packages of non-DD
who are part of the team. Sponsorship requests can be sent on the main
discussion list or on #debian-python IRC channel (OFCT network).

All team members should of course follow the main discussion list:
debian-python@lists.debian.org (DP_listinfo_)

--------------
Maintainership
--------------

A package maintained within the team should have the name of the team either in
the Maintainer field or in the Uploaders field.

Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>

This enables the team to have an overview of its packages on the tracker_ website.

Thus if you bring some packages into the team, you can keep your name in the
Maintainer field. You will receive bug reports and handle your package as usual
except that other team members may help from time to time and/or take over when
you're too busy.

If you put the team in the Maintainer field, the package will be handled
completely by the team and every member is invited to work on any outstanding
issue.

Team members who have broad interest should subscribe to the mailing list
python-apps-team@lists.alioth.debian.org whereas members who are only
interested in some packages should use the Package Tracking System to follow
the packages.

-----------------
Quality Assurance
-----------------

The goal of the team is to maintain all packages as best as possible. Thus
every member is encouraged to do general QA work on all the packages: fix bugs,
test packages, improve them to use the latest Python packaging tools, etc.

-------
License
-------

Copyright (c) 2007 Python Applications Packaging Team. All rights reserved.
This document is free software; you may redistribute it and/or modify it under
the same terms as GNU GPL v2 or later.

.. _tracker: https://tracker.debian.org/teams/python-apps/
.. _GIT_repository: https://salsa.debian.org/python-team/applications
.. _mailing_list: http://lists.alioth.debian.org/mailman/listinfo/python-apps-team
.. _debian-python: mailto:debian-python@lists.debian.org?Subject=PAPT:%20join%20request
.. _member_list: https://salsa.debian.org/groups/python-team/applications/-/group_members
.. _DP_listinfo: http://lists.debian.org/debian-python/
